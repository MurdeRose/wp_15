##
Nagłówek

*Tu paragraf z kursywą*

** Tu paragraf z pogrubieniem**

~A tutaj przekreślenie~

>Tutaj jest blok cytatu

Tu zrobimy listę:
1. Pierwszy
2. Drugi
3. Trzeci

Teraz nieuporządowana:
- One
- Two
- Three


```py
a = 1
b = 2
print('Tutaj jest poprawny kod')
```

A tutaj zagnieżdżony:``(print('Hello World!')``

Na koniec zdjęcie:
![kitten-4611189_1280.jpg](kitten-4611189_1280.jpg)
