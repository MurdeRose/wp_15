# Najpierw tabelka :money_with_wings: {#n1}
|kolumna 1 | kolumna 2 | kolumna 3 |
|:---:     |:---:      |:---:      |
|    a     |     c     |     e     |
|    b     |     d     |     f     |

## Tutaj Kod :alien: {#n2}
```py
num = 34
litera = 'a'
print('Tekst')
```
### Lista Zadań :point_down: {#n3}
- [X] wstać
- [X] wyjść z domu
- [ ] odzyskać chęci do życia

#### Footnotes
[Tabelka](#n1)
[Kod](#n2)
[Lista](#n3)


